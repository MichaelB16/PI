-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 29/11/2017 às 17:25
-- Versão do servidor: 10.1.21-MariaDB
-- Versão do PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `igreja`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `aviso`
--

CREATE TABLE `aviso` (
  `id_aviso` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `data` date NOT NULL,
  `aviso` text NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `aviso`
--

INSERT INTO `aviso` (`id_aviso`, `id_user`, `data`, `aviso`, `data_criacao`) VALUES
(12, 1, '2017-11-24', 'teste aviso', '2017-11-18 16:55:12'),
(13, 1, '2017-11-29', 'oi', '2017-11-18 17:01:37'),
(14, 1, '2017-11-28', 'teste', '2017-11-18 17:02:39'),
(15, 1, '2017-11-28', 'este ok', '2017-11-18 17:03:25'),
(16, 1, '2017-11-30', 'teste', '2017-11-29 15:22:15');

-- --------------------------------------------------------

--
-- Estrutura para tabela `chamada`
--

CREATE TABLE `chamada` (
  `id_chamada` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_senhora` int(11) NOT NULL,
  `data` date NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` char(1) NOT NULL COMMENT 'Falta => 0 , Presente => 1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `chamada`
--

INSERT INTO `chamada` (`id_chamada`, `id_user`, `id_senhora`, `data`, `data_criacao`, `status`) VALUES
(32, 1, 32, '2017-11-04', '2017-11-04 17:33:00', '1'),
(33, 1, 33, '2017-11-04', '2017-11-04 17:33:00', '1'),
(34, 1, 34, '2017-11-04', '2017-11-04 17:33:01', '1'),
(35, 1, 32, '2017-11-06', '2017-11-06 00:52:59', '1'),
(36, 1, 33, '2017-11-06', '2017-11-06 00:52:59', '1'),
(37, 1, 34, '2017-11-06', '2017-11-06 00:52:59', '1'),
(38, 1, 32, '2017-11-07', '2017-11-07 15:11:14', '0'),
(39, 1, 33, '2017-11-07', '2017-11-07 15:11:14', '0'),
(40, 1, 34, '2017-11-07', '2017-11-07 15:11:14', '0'),
(41, 1, 32, '2017-11-08', '2017-11-08 03:56:16', '0'),
(42, 1, 33, '2017-11-08', '2017-11-08 03:56:16', '0'),
(43, 1, 34, '2017-11-08', '2017-11-08 03:56:16', '0'),
(44, 1, 32, '2017-11-09', '2017-11-09 03:28:50', '1'),
(45, 1, 33, '2017-11-09', '2017-11-09 03:28:50', '1'),
(46, 1, 34, '2017-11-09', '2017-11-09 03:28:50', '1'),
(47, 1, 32, '2017-11-10', '2017-11-10 01:56:50', '1'),
(48, 1, 33, '2017-11-10', '2017-11-10 01:56:50', '0'),
(49, 1, 34, '2017-11-10', '2017-11-10 01:56:50', '0'),
(50, 1, 35, '2017-11-10', '2017-11-10 01:56:50', '0');

-- --------------------------------------------------------

--
-- Estrutura para tabela `evento`
--

CREATE TABLE `evento` (
  `id_evento` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `evento` text NOT NULL,
  `data` date NOT NULL,
  `local` text NOT NULL,
  `descricao` text NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `evento`
--

INSERT INTO `evento` (`id_evento`, `id_user`, `evento`, `data`, `local`, `descricao`, `data_criacao`) VALUES
(4, 1, 'Culto Feminino ', '2017-11-17', 'Igreja Batista - Barra do CearÃ¡', 'teste', '2017-11-09 04:10:09');

-- --------------------------------------------------------

--
-- Estrutura para tabela `senhora`
--

CREATE TABLE `senhora` (
  `id_senhora` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nome` text NOT NULL,
  `data_nascimento` date NOT NULL,
  `telefone` varchar(20) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `senhora`
--

INSERT INTO `senhora` (`id_senhora`, `id_user`, `nome`, `data_nascimento`, `telefone`, `data_criacao`) VALUES
(32, 1, 'Maria', '1980-11-04', '(54)35435-3453', '2017-11-04 17:31:57'),
(33, 1, 'Celiene', '1990-10-12', '(76)75757-6567', '2017-11-04 17:31:57'),
(34, 1, 'Rita', '1965-09-20', '(86)78676-6876', '2017-11-04 17:31:57'),
(35, 1, 'Simone', '1985-11-09', '(23)47683-7264', '2017-11-09 03:29:51'),
(36, 1, 'julia', '1970-07-11', '(45)34534-5345', '2017-11-10 03:16:19');

-- --------------------------------------------------------

--
-- Estrutura para tabela `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `user`
--

INSERT INTO `user` (`id_user`, `user`, `password`, `email`, `phone`, `create`) VALUES
(1, 'mica', '202cb962ac59075b964b07152d234b70', 'michael@fortics', '(85)8915-3375', '2017-10-09 00:33:48');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `aviso`
--
ALTER TABLE `aviso`
  ADD PRIMARY KEY (`id_aviso`);

--
-- Índices de tabela `chamada`
--
ALTER TABLE `chamada`
  ADD PRIMARY KEY (`id_chamada`),
  ADD KEY `id_user` (`id_user`,`id_senhora`),
  ADD KEY `id_senhora` (`id_senhora`);

--
-- Índices de tabela `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id_evento`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `senhora`
--
ALTER TABLE `senhora`
  ADD PRIMARY KEY (`id_senhora`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices de tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `aviso`
--
ALTER TABLE `aviso`
  MODIFY `id_aviso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de tabela `chamada`
--
ALTER TABLE `chamada`
  MODIFY `id_chamada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT de tabela `evento`
--
ALTER TABLE `evento`
  MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `senhora`
--
ALTER TABLE `senhora`
  MODIFY `id_senhora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `chamada`
--
ALTER TABLE `chamada`
  ADD CONSTRAINT `id_senhora` FOREIGN KEY (`id_senhora`) REFERENCES `senhora` (`id_senhora`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `id_usuario` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Restrições para tabelas `senhora`
--
ALTER TABLE `senhora`
  ADD CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
