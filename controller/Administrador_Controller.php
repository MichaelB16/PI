<?php 

	class Administrador_Controller extends Lb_Controllers{

		public function init(){
			$this->title = "Bem vindo á tela de Administrador";
			$this->icon = "<i class='icon user'></i>";
			$this->painel = "Administrador";
			$this->User = new Usuario_Base();
			$this->Admin = new Administrador_Base();
		}

		public function index(){
			$id = base64_decode($this->_GET("id"));
			if($id){
				$consulta = $this->User->find($id);
				$this->user = $consulta['user'];
				$this->email = $consulta['email'];
				$this->password = $consulta['password'];
				$this->igreja = $consulta['igreja'];
				$this->phone = $consulta['phone'];
				$this->id = $this->_GET("id");
			}else{
				$this->user = "";
				$this->phone = "";
				$this->email = "";
				$this->igreja = "";
				$this->password = "";
				$this->id = "";
			}
		}

		public function addUsers(){
			$igreja = $this->_POST("igreja");
			$user = $this->_POST("user");
			$email = $this->_POST("email");
			$password = md5($this->_POST("password"));
			$phone = $this->_POST("phone");
			$id = base64_decode($this->_POST("id"));
			
			$lista = ["user"=>$user,"email"=>$email,"phone"=>$phone,"igreja"=>$igreja,"password"=>$password];

			if($id){
				$this->User->update($lista,$id);
			}else{
				$this->User->insert($lista);
			}

			$this->redirect($this->url(['action'=>"index"]));
		}

		public function remove(){
			$id = base64_decode($this->_GET("id"));
			
			if($id){
				$this->User->delete($id);
			}
		}

	} 


 ?>