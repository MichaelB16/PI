<?php 

	class Home_Controller extends Lb_Controllers{

		public function init(){
			$this->title = "ACP - Seja Bem Vindo";
			$this->painel = "Dashboard";
			$this->icon = "icon dashboard";
			$this->id_user = $_SESSION['id'];
			$this->Senhora = new Senhora_Base();
			$this->Chamada = new Chamada_Base();
			$this->Evento  = new Evento_Base();
			$this->Aviso   = new Aviso_Base();


		}
		
		public function index(){
			

			if($this->id_user != false){
				$consulta = $this->Senhora->fetch("id_user=".$this->id_user);
				$this->count = count($consulta);

				$query = $this->Evento->fetch("id_user=".$this->id_user);
				$this->count_event = count($query);
			}			

		}

		protected static function DateSQL($date){
			return implode("-",array_reverse(explode("/",$date)));
		}

		public function Cadastro_Aviso(){
			$data = self::DateSQL($this->_POST("data"));
			$aviso = $this->_POST("aviso");

			$lista = ["id_user"=>$this->id_user,"aviso"=>$aviso,"data"=>$data];

			$id = $this->Aviso->insert($lista);

			echo json_encode(["check"=>"success","id"=>base64_encode($id)]);
			exit;
		}

		public function removeAviso(){
			$id = base64_decode($this->_POST("id"));
			$status = "error";
			if($id){
				$this->Aviso->delete($id);
				$status = "success";
			}
			echo $status;
			exit;

		}

	}



 ?>