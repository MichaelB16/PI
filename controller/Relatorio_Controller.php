<?php

    class Relatorio_Controller extends Lb_Controllers{

        public function init(){
            $this->title = "Acp - Relatório de Chamadas";
            $this->painel = "Relatório de Chamadas";
            $this->icon = "icon file";
        }

        public function index(){
            // Data
            $sql = $this->_pdo->query("SELECT DISTINCT date_format(data , '%m') as data   FROM `chamada`");
            $this->consulta = $sql->fetchAll(PDO::FETCH_ASSOC);

            // senhpra
            $query = $this->_pdo->query("select * from senhora");
            $this->senhora = $query->fetchAll(PDO::FETCH_ASSOC);

            $meses = array('Janeiro','Fevereiro', 'Março', 'Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');


        }

        public function search(){
            $this->index();
            $tipo = ($this->_GET("tipo") != "") ? "and c.status ='".$this->_GET("tipo")."'" : "" ;
           
            $membro = ($this->_GET("membro") != "") ? "and c.id_senhora='".$this->_GET("membro")."'" : "";
            $data = $this->_GET("data");

            $sql = $this->_pdo->query("select c.*,s.nome from chamada c inner join senhora s on s.id_senhora = c.id_senhora where c.data like'%$data%' $membro $tipo order by s.nome");
           
            $this->result = $sql->fetchAll(PDO::FETCH_ASSOC);
        }

    }



?>