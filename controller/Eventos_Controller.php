<?php 

    class Eventos_Controller extends Lb_Controllers{

        public function init(){
            $this->title = "Eventos";
            $this->icon = "icon calendar";
            $this->painel = "Eventos";
            $this->Evento = new Evento_Base();
            $this->id_user = $_SESSION['id'];
        }

        protected static function DateSQL($date , $type = null){
            if($type == "br")
                 $data = date("d/m/Y",strtotime($date));  
            else
                 $data = implode("-",array_reverse(explode("/",$date)));
                 
            return $data;      
        } 


        public function index(){
            $id = base64_decode($this->_GET("id"));

            if($id){
                $consulta = $this->Evento->find($id);
                $this->evento = $consulta['evento'];
                $this->local = $consulta['local'];
                $this->descricao = $consulta['descricao'];
                $this->data = self::DateSQL($consulta['data'],"br");
            }else{
                $this->evento = "";
                $this->local = "";
                $this->descricao = "";
                $this->data = date("d/m/Y");
            }
        }

         

        public function cadastro(){
            $id = base64_decode($this->_POST("id"));
            $evento = $this->_POST("evento");
            $local = $this->_POST("local");
            $descricao = $this->_POST("descricao");
            $data = self::DateSQL($this->_POST("data"));
        
            $lista = ["evento"=>$evento,"id_user"=>$this->id_user,"local"=>$local,"data"=>$data,"descricao"=>$descricao];

            if($id){
                $state = "up";
                $this->Evento->update($lista,$id);
            }else{
                $state = "success";
                $this->Evento->insert($lista);
            }

            $this->redirect($this->url(["action"=>"index","state"=>$state]));
        }    

        public function delete(){
            $id = base64_decode($this->_POST("id"));

            if($id){
                $this->Evento->delete($id);
            }
              
            $this->redirect($this->url(["action"=>"index"]));
        }

    }

?>