<?php

Class Index_Controller extends Lb_Controllers{

  public function init(){
    $this->title = "Login  de Usuário ";
    $this->Usuario = new Usuario_Base();
  }

  public function index(){
    $this->_layout = "login";
  }

  private static function _session($dados,$tipo){
    $_SESSION['user'] = $dados['user'];
    $_SESSION['email'] = $dados['email'];
    $_SESSION['phone']= $dados['phone'];
    $_SESSION['tipo'] = $tipo;

    if($tipo == "user")
        $_SESSION['id'] = $dados["id_user"];
    else
        $_SESSION['id_admin'] = $dados["id_admin"];
  }

  private function Auth($user , $password){
    $sql = $this->_pdo->query("select * from administrador where user='$user' and password='$password' limit 1");
    $row = $sql->rowCount();
    
    if($row > 0 ){
      $dados = $sql->fetch(PDO::FETCH_ASSOC);
      self::_session($dados,"admin"); 
      $this->redirect($this->url(['controller'=>"Administrador","action"=>"index"])); 
    } 

    $consulta = $this->_pdo->query("select * from user where user='$user' and password='$password' limit 1");    
    $count = $consulta->rowCount();
    
    if($count > 0){
      $dados = $consulta->fetch(PDO::FETCH_ASSOC);
      self::_session($dados , "user");
      $this->redirect($this->url(['controller'=>"Home","action"=>"index"]));
    }else{
      $this->redirect($this->url(['action'=>'index',"access"=>"error"]));
    }    
  }

  public function signin(){
    $user = $this->_POST("user");
    $password = md5($this->_POST("password"));
    $this->Auth($user,$password);
  }

  public function access_admin(){
      $user = $this->_POST("user");
      $password = $this->_POST("password");
      $this->Auth($user,$password);
  }

  public function logout(){
    unset($_SESSION);
    session_destroy();
    $this->redirect($this->url(['action'=>'index']));
  }

}

?>
