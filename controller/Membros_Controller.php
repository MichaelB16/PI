<?php 

class Membros_Controller extends Lb_Controllers{

	public function init(){
		$this->title = "Relatório  de membros";
		$this->painel = "Relatorio de Membros";
		$this->icon = "icon file";	
		$this->id_user = $_SESSION['id'];
		$this->meses = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
	}

	public function index(){
		$this->mes = date("m");
	}

	public function search(){
		$this->index();
		$membros = $this->_GET("membro");
			//$mes = ($this->_GET("anivesariante") != "") ? "and data_nascimento='".$this->_GET("anivesariante")."'" : "";

			// "select * from senhora where id_user='$this->id_user' $mes order by data_nascimento,nome";
		
		$this->sql = $this->_pdo->query("select * from senhora where id_user='$this->id_user'  order by data_nascimento,nome")->fetchAll(PDO::FETCH_ASSOC);

	}

	public function export(){
		header("Content-type: application/vnd.ms-excel");   
		header("Content-type: application/force-download");
		header("Content-Disposition: attachment; filename=membros.xls");  
		
		$this->sql = $this->_pdo->query("select * from senhora where id_user='$this->id_user'  order by data_nascimento,nome")->fetchAll(PDO::FETCH_ASSOC);
		
		echo '<table border="1">';
			echo '<thead>';
				echo '<tr>';
						echo '<td>Nome</td>';
						echo '<td>Data de Nascimento</td>';
						echo '<td>Telefone</td>';
				echo '</tr>';
			echo '</thead>';
			echo '<tbody>';
				foreach($this->sql as $value):
					echo '<tr>';
						echo '<td>'.$value['nome'].'</td>';
						echo '<td>'.str_replace("-","/",$value['data_nascimento']).'</td>';
						echo '<td>'.$value['telefone'].'</td>';
					echo '</tr>';
				endforeach;
			echo '</tbody>';
		echo '</table>';
		exit();
	}

}

?>