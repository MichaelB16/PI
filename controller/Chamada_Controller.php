<?php 

	class Chamada_Controller extends Lb_Controllers {

		public function init(){
			$this->title = "ACP - Chamadas";
			$this->painel = "Chamada";
			$this->icon = "";
			$this->id_user = $_SESSION['id'];
			$this->Senhora = new Senhora_Base();
			$this->Chamada =  new Chamada_Base();
		}

		public function index(){
			
		}

		public function realizar_chamada(){
			$id = $this->_POST("id");
			$status = $this->_POST("situacao");
			$data = date('Y-m-d');
			// array de armazenamento de valores
			$Lista = [];

			// verifica se já houver a chamada pela data
			$sql = $this->_pdo->query("select * from chamada where data='$data'");
			
			if($sql->rowCount() > 0 ){
				$state = "error";
			}else{
				$state = "success";	
				foreach($status as $i=>$value):
					array_push($Lista, ["id_user"=>$this->id_user,"data"=>$data,"id_senhora"=>$id[$i],"status"=>$value]);
					$this->Chamada->insert($Lista[$i]);
				endforeach;	
			}
			$this->redirect($this->url(['action'=>"index","state"=>$state]));
		}

	}
	
	

 ?>