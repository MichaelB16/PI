<?php 


class Senhoras_Controller extends Lb_Controllers{

	public function init(){
		$this->title = "ACP - Cadastro de Senhoras";
		$this->painel = "Cadastro de Senhoras";
		$this->icon = "icon plus circle";
		$this->id_user = $_SESSION['id'];
		$this->Senhoras = new Senhora_Base();
	}

	protected static function DateSQL($date , $type = null){
		if($type == "br")
			$data = date("d/m",strtotime($date));
		else
			$data =  implode("-",array_reverse(explode("/",$date)));

		return $data;	
	}


	public function index(){
		$id = base64_decode($this->_GET("id"));
		if($id != false):
			$consulta = $this->Senhoras->find($id);
			$data = $consulta['data_nascimento'];
			$this->nome = $consulta['nome'];
			$this->data = $data;
			$this->phone = $consulta['telefone'];
			$this->id = base64_encode($consulta['id_senhora']);
		else:
			$this->nome = "";
			$this->data = date("d/m");
			$this->phone = "";
			$this->id = "";	
		endif;

		$this->html = $this->Pagination("senhora",10,"id_user=$this->id_user");

	}



	public function register(){
		$id = base64_decode($this->_POST("id_senhora"));
		$nome = $this->_POST("nome");
		$data = $this->_POST('data_nascimento');
		$phone = $this->_POST("telefone");


		foreach($nome as $i=>$value){
			$datas = self::DateSQL($data[$i]);
			$lista = ["id_user"=>$this->id_user,"nome"=>$value,"data_nascimento"=>$datas,"telefone"=>$phone[$i]];

			if($id != false){
				$this->Senhoras->update($lista,$id);
			}else{
				$this->Senhoras->insert($lista);
			}

		}

		$this->redirect($this->url(["action"=>"index"]));
	}

	public function delete(){
		$id = base64_decode($this->_POST("id"));

		if($id){
			$this->Senhoras->delete($id);
			echo $this->Senhoras->getSQL();
		}

		$this->redirect($this->url(['action'=>'index']));
	}


}










?>