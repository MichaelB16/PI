$(function(){
	// atribuindo autocomplete off nos inputs
	$("input").attr("autocomplete","off");
	// dropdown semamtic ui
	$(".ui.dropdown").dropdown();
	// checkbox semantic ui
	$(".ui.checkbox").checkbox();
	// tooltip
	$("*[data-content]").popup();
	// accordion semantic ui
	$('.ui.accordion').accordion();
	// mascara
	$('.phone').mask("(00)90000-0000");
	$(".date-mask").mask("00/00");
	$('.data').mask("00/00/0000", {placeholder: "__/__/____"});

	// dataícker jquery
	$(".data").datepicker({
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'
	});
	// selecionar todos os itens
	$(document).on("click","[data-toggle='check_all']",function(){
		$("table").find("tbody input[type='checkbox']").prop("checked" , true);
	});
	// desmarcar todos os itens
	$(document).on("click","[data-toggle='uncheck']",function(){
		$("table").find("tbody input[type='checkbox']").prop("checked" , false);
	});
	// excluir todos os itens
	$(document).on("click","[data-toggle='del_check']",function(){
		var checkbox = $("table").find("tbody input[type='checkbox']:checked");
		
		if(checkbox){
			swal({   
				title: "Você tem certeza ?",
				text: "Você realmente de excluir !", 
				type: "warning",   
				showCancelButton: true, 
				confirmButtonColor: "#DD6B55", 
				confirmButtonText: "Excluir",
				cancelButtonText: "Cancelar", 
				closeOnConfirm: false,  
				closeOnCancel: false }, 
				function(isConfirm){   
					if (isConfirm) { 
						swal({
							title:"Deletando!",
							text:'<i class="icon spinner loading"></i>',
							type: "success",
							html:true,
							showConfirmButton:false, 
							timer:1000
						});

						$.each(checkbox,function(){
							$(this).closest("tr").remove();
							var url = $(this).closest(".check-table").closest('td').find('.delete_confirm').attr("href");
							$.post(url,{id:$(this).val()});
						});
					} else {  
						swal("Cancelado", "Cancelado com sucesso !", "error");   
					} 
				});
		}
		
	});
	// função de confirmação  da exclusão
	$(document).on("click",".delete_confirm",function(){
		var id = $(this).attr("data-id");
		var url = $(this).attr("href");
		
		swal({   
			title: "Você tem certeza ?",
			text: "Você realmente de excluir !", 
			type: "warning",   
			showCancelButton: true, 
			confirmButtonColor: "#DD6B55", 
			confirmButtonText: "Excluir",
			cancelButtonText: "Cancelar", 
			closeOnConfirm: false,  
			closeOnCancel: false }, 
			function(isConfirm){   
				if (isConfirm) {     
					$.post(url, {"id":id} , function(data){
						swal({
							title:"Deletando !",
							text:"<i class='spinner loading icon'></i>",
							showConfirmButton:false,
							html:true,
							type:"success" 
						});
						//alertify.success("<b class='color'><i class='icon checkmark'></i> ExcluÃ­do com sucesso !</b>");
						setTimeout(function(){
							location.reload();
						},1000);
					})  
				} else {  
					swal("Cancelado", "Cancelado com sucesso !", "error");   
				} 
			});

		return false;
	});

});